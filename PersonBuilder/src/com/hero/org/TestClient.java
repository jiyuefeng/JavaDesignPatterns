package com.hero.org;

public class TestClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ConcreteBuilder builder = new ConcreteBuilder();
		PersonDirector director = new PersonDirector(builder);
		director.construct();
		Person person = director.getPerson();
		System.out.println("person-->>" + person);
	}

}
