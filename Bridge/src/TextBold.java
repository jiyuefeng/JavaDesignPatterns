/**
 *  The RefinedAbstraction
 */

public class TextBold extends Text {
	private TextImp imp;
	public TextBold(Class<?> c) {
		imp = GetTextImp(c);
	}
	public void DrawText(String text) {
		System.out.println(text);
		System.out.println("The text is bold text!");
		imp.DrawTextImp();
	}
}