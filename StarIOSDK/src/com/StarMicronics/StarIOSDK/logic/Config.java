package com.StarMicronics.StarIOSDK.logic;

import java.io.File;

import android.os.Environment;

public class Config {
	public static boolean bFileOperationInSDCard = true; // true：文件保存在SD卡；false：保存在/data/data/<myAppPackage
															// name>/files目录；原因：真机系统文件需要root才能看到
	public static File sdcardRootPath = Environment.getExternalStorageDirectory();
	public static final String fileOperationInSDCardPath = sdcardRootPath + "/york_it/project/ordering/";// SD卡下指定保存的路径；
	public static final String ImageCachePath = "/sdcard" + fileOperationInSDCardPath; // 图片及数据缓存路径(不包括用户数据)
	public static final String mediaCachePath = "/sdcard/york_it/project/media/"; // 视频文件路径
	public static final String downPath = "/sdcard/york_it/project/media/download/"; // 视频下载文件路径
	public static final String xmlPath = "/sdcard/api/cc/xml/";

	public static final int CONNECT_TIMEOUT = 40 * 1000; // 单位：ms
	public static final int READ_TIMEOUT = 40 * 1000; // 单位：ms

	public static final int LISTVIEW_IMAGEBUF_MAX = 12; // 图片缓存个数

	public static final int groupMemberCount = 9; // 每个餐厅的属性个数，跟随协议变动

	public static final String REGULAR_EXPRESSION_0 = "<`>"; // 分隔符，别乱改
	public static final String REGULAR_EXPRESSION_1 = "<~>"; // 分隔符，别乱改

	public static final String COLLECTION_FILE = "SysFile_Collection"; // 收藏-数据文件名
	public static final String OERDERING_FILE = "SysFile_Ordering"; // 预订-数据文件名

	/**
	 * custom config
	 */
	public static boolean ifNetworkConnection = true; // 网络连接是否正常
	public static String format = ".jpg"; // 图片格式， 后缀名
	public static String AUDIO_SUFFIX = ".m4a"; // 录音格式， 后缀名
	public static String VIDEO_SUFFIX = ".mp4"; // 视频格式， 后缀名

	public static String userDataPath = MyApplication.context.getFilesDir().toString(); // 得到用户信息(系统路径)
	public static String pwdPath = MyApplication.context.getFilesDir().toString(); // 得到用户密码(系统路径)
	public static String path_IP = MyApplication.context.getFilesDir().toString(); // IP路径
}
