package com.StarMicronics.StarIOSDK.logic;

import util.Util_File;
import android.app.Activity;
import android.app.Application;
import android.content.Context;

public class MyApplication extends Application {
	public static MyApplication instance;
	public static Activity CURRENT_ACTIVTY; // 目前的页面
	public static Context context;

	public static MyApplication getInstance() {
		return instance;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		instance = this;
		context = this;
		Util_File.checkFileOperationInSDCard();
	}
}
