package com.StarMicronics.StarIOSDK;

import java.util.Hashtable;

import util.Util_Convert;
import util.Util_File;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
public class PrintQrCodeImage extends Activity {

	Bitmap bitmap = null;
	EditText editText;
	ImageView imageView_Image;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.printingtextasimage_);
		editText = (EditText) findViewById(R.id.editText1);
		imageView_Image = (ImageView) findViewById(R.id.imageView_Image);
	}

	public void PrintText(View view) {

		if (bitmap != null) {
			int paperWidth = 408; // 2inch (408 dot)
			boolean compressionEnable = true;
			String portName = PrinterTypeActivity.getPortName();
			String portSettings = PrinterTypeActivity.getPortSettings();
			// PrinterFunctions.PrintBitmapImage(this, portName, portSettings,
			// getResources(), source, 576, compressionEnable);
			Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.image1);
			System.out.println("bitmap->>" + bitmap.getByteCount() + "  " + bitmap.getWidth() + "   " + bitmap.getHeight());
			System.out.println("bm->>" + bm.getByteCount() + "  " + bm.getWidth() + "   " + bm.getHeight());
			// PrinterFunctions.PrintBitmapImage(this, portName, portSettings,
			// getResources(), R.drawable.qrcode, 576, compressionEnable);

			PrinterFunctions.PrintBitmapImage(this, portName, portSettings, bitmap, paperWidth, true);

		} else {
			Toast.makeText(this, "请生成二维码！", 0).show();
		}
	}

	public void Help(View view) {
		String message = editText.getText().toString();
		if (!TextUtils.isEmpty(message)) {
			try {
				bitmap = Create2DCode(message);
				// bitmap =
				// ThumbnailUtils.extractThumbnail(Create2DCode(message), 300,
				// 300);
				System.out.println("bitmap->>" + bitmap.getByteCount());
				imageView_Image.setImageBitmap(bitmap);
				Util_File.writeFile(Util_Convert.Bitmap2Bytes(bitmap), "qrcode.png", Context.MODE_PRIVATE);
			} catch (WriterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			Toast.makeText(this, "请输入要生成的信息！", 0).show();
		}
	}

	

	/**
	 * 用字符串生成二维码
	 * 
	 * @param str
	 * @author zhouzhe@lenovo-cw.com
	 * @return
	 * @throws WriterException
	 */
	public Bitmap Create2DCode(String str) throws WriterException {
		final int WHITE = 0xFFFFFFFF; // 可以指定其他颜色，让二维码变成彩色效果
		final int BLACK = 0xFF000000;
		int QR_WIDTH = 250;
		int QR_HEIGHT = 250;
		// 生成二维矩阵,编码时指定大小,不要生成了图片以后再进行缩放,这样会模糊导致识别失败
		BitMatrix matrix = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, QR_WIDTH, QR_HEIGHT);
		int width = matrix.getWidth();
		int height = matrix.getHeight();
		// 二维矩阵转为一维像素数组,也就是一直横着排了
		int[] pixels = new int[width * height];
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (matrix.get(x, y)) {
					pixels[y * QR_WIDTH + x] = BLACK;
				} else {
					pixels[y * QR_WIDTH + x] = WHITE;
				}
			}
		}
		Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		// 通过像素数组生成bitmap,具体参考api
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
		return bitmap;
	}
}
