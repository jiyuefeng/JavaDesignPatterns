package util;

import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.StarMicronics.StarIOSDK.logic.MyApplication;

public class Util_APN {

	public static String netConnectType = "";

	/**
	 * 检测网络是否连接（注：需要在配置文件即AndroidManifest.xml加入权限） <uses-permission
	 * android:name="android.permission.ACCESS_NETWORK_STATE"></uses-permission>
	 * 
	 * @param context
	 * @return true : 网络连接成功
	 * @return false : 网络连接失败
	 * */
	public static boolean checkNetConnect(Context context) {

		// 获取手机所有连接管理对象（包括对wi-fi,net等连接的管理）
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			// 获取网络连接管理的对象
			NetworkInfo info = connectivity.getActiveNetworkInfo();
			if (info != null && info.isAvailable() && info.getState() == NetworkInfo.State.CONNECTED) {
				// 判断当前网络是否已经连接
				return true;
			}
		}
		return false;
	}

	/**
	 * 检查WIFI是否打开
	 * 
	 * @param context
	 * @return
	 */
	public static boolean checkWifi() {
		Context context = MyApplication.getInstance();
		WifiManager mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
		int ipAddress = wifiInfo == null ? 0 : wifiInfo.getIpAddress();
		if (mWifiManager.isWifiEnabled() && ipAddress != 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 获取当前APN，（转成小写） 获取联网方式：APN
	 * 通过context取得ConnectivityManager中的NetworkInfo里关于apn的联网信息
	 * 
	 * @param context
	 * @return apn; 例如："cmwap" "cmnet"
	 */
	public static String getCurAPN() {
		String apn = null;
		try {
			Context context = MyApplication.getInstance();
			// 通过context得到ConnectivityManager连接管理
			ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			// 通过ConnectivityManager得到NetworkInfo网络信息
			NetworkInfo info = manager.getActiveNetworkInfo();
			// 获取NetworkInfo中的apn信息
			if (info != null) {
				apn = info.getExtraInfo().toLowerCase();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return apn;
	}

	/**
	 * 自动判断当前是否需要使用代理
	 * 
	 * @return true : 使用代理
	 * @return false: 不使用代理
	 */
	public static boolean getNetProxy() {

		if (checkWifi()) {// 判断是否是wify方式，如是，直接用net方式(即非代理)，不用管什么卡；
			netConnectType = "wifi";
			// Util_G.DisplayToast("使用WIFI联网！", Toast.LENGTH_LONG);
			return false;
		}

		/**
		 * 判断当前APN接入点名称，如果包含“wap”字符，则走wap(即代理)方式，不用管什么卡；
		 * 如果包含“net”字符，则走net(寄非代理)方式，不用管什么卡；
		 * */
		String apn = getCurAPN();
		if (apn == null)
			return false;// 默认不使用代理
		if (apn.indexOf("wap") != -1) {
			netConnectType = "wap";
			// Util_G.DisplayToast("使用代理(wap)联网！", Toast.LENGTH_LONG);
			return true;
		}
		if (apn.indexOf("net") != -1) {
			netConnectType = "net";
			// Util_G.DisplayToast("使用非代理(net)联网！", Toast.LENGTH_LONG);
			return false;
		}
		return false;// 默认不使用代理
	}

	/** 网关 */
	public static String getNetProxyStr() {
		String proxyStr = "10.0.0.200";// 电信 ctwap
		try {
			Uri uri = Uri.parse("content://telephony/carriers/preferapn"); // 获取当前正在使用的APN接入点
			Cursor mCursor = MyApplication.getInstance().getContentResolver().query(uri, null, null, null, null);
			if (mCursor != null) {
				mCursor.moveToNext(); // 游标移至第一条记录，当然也只有一条
				proxyStr = mCursor.getString(mCursor.getColumnIndex("proxy"));
			}
			mCursor.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("proxyStr-->>" + proxyStr);
		}
		return proxyStr;
	}

}
