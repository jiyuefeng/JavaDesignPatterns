package com.hero.org;

/***
 * 这个类为简单工厂类
 * 
 * @author herozhou1314
 * 
 */
public class DataAccess {
	// String db = "com.hero.org." + "Access";
	String db = "com.hero.org." + "SqlServer";
	public IUser createUser() {
		// 定义一个水果对象
		String clazz = db.concat("User");
		IUser window = null;
		try {
			window = (IUser) Class.forName(clazz).newInstance();
		} catch (Exception e) {
			System.out.println("创建出错");
		}
		return window;
	}

	public IDepartment createDepartment() {
		// 定义一个水果对象
		IDepartment window = null;
		String clazz = db.concat("Department");
		try {
			window = (IDepartment) Class.forName(clazz).newInstance();
		} catch (Exception e) {
			System.out.println("创建出错");
		}
		return window;
	}
}
