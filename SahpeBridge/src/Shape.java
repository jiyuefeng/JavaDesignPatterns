public abstract class Shape {
	ColorManage manage;
	protected ColorManage GetRenderingImp(Class<?> c) {
		ColorManage text = null;
		try {
			text = (ColorManage) Class.forName(c.getName()).newInstance();
		} catch (Exception e) {
			System.out.println("创建出错");
		}
		return text;
	}

	public abstract void DrawText(String text);
}
