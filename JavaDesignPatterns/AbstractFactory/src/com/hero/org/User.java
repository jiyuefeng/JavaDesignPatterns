package com.hero.org;

public class User {
	private String _name;
	private int _id;

	public String getName() {
		return _name;
	}

	public void setName(String user) {
		this._name = user;
	}

	public long getid() {
		return _id;
	}

	public void setid(int _id) {
		this._id = _id;
	}

	@Override
	public String toString() {
		return "User [_name=" + _name + ", _id=" + _id + "]";
	}
}
