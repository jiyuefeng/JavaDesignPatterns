package com.hero.org;

public class Client {

	public static void main(String[] args) {
		System.out.println("===================java抽象工厂=====================");
		User user = new User();
		IFactory factory = new AccessFactory();
		IUser iUser = factory.createUser();
		user = iUser.getUser(11);

		System.out.println("user-->>" + user);
		factory = new SqlServerFactory();
		iUser = factory.createUser();
		user = iUser.getUser(1222);
		System.out.println("user-->>" + user);

		// DataAccess dataSccess = new DataAccess();
		IUser iUser1 = factory.createUser();
		System.out.println("user-->>" + iUser1.getUser(8996));
		IDepartment department = factory.createDepartment();
		// IDepartment department =
		// dataSccess.createDepartment(SqlServerDepartment.class);
		System.out.println("user-->>" + department.getDepartment(33444));
		
		
		System.out.println("===================java 反射机制=====================");
		DataAccess access = new DataAccess();
		department = access.createDepartment();
		System.out.println("user-->>" + department.getDepartment(675757575));
		iUser = access.createUser();
		user = iUser.getUser(678668686);
		System.out.println("user-->>" + user);
	}
}
