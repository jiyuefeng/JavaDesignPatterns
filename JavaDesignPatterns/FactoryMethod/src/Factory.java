public class Factory {
	public Window CreateWindow(String type) {

		if (type.equals("Big")) {
			return new WindowBig();
		} else if (type.equals("Small")) {
			return new WindowSmall();
		} else {
			return new WindowBig();
		}
	}

	public Window CreateWindow(Class c) {
		// 定义一个水果对象
		Window window = null;
		try {
			window = (Window) Class.forName(c.getName()).newInstance();
		} catch (Exception e) {
			System.out.println("创建出错");
		}
		return window;
	}

	// The Main function only for our test
	public static void main(String[] args) {
		Factory myFactory = new Factory();
		Window myBigWindow = myFactory.CreateWindow("Big");
		myBigWindow.func();
		Window mySmallWindow = myFactory.CreateWindow("Small");
		mySmallWindow.func();

		myBigWindow = myFactory.CreateWindow(WindowBig.class);
		myBigWindow.func();
		mySmallWindow = myFactory.CreateWindow(WindowSmall.class);
		mySmallWindow.func();
	}
}