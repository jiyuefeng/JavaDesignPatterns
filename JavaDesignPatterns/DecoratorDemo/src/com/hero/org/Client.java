package com.hero.org;

public class Client {

	public static void main(String[] args) {
		CorecterComponent component = new CorecterComponent();
		Component component2 = new CorecterDecoratorA(component);
		component2.watchTv();
		component2 = new CorecterDecoratorB(component);
		component2.watchTv();
	}
}
