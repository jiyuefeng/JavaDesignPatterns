package com.hero.org;

public class ConcreteStateD extends State {
	@Override
	public void Handle(Context context) {
		System.out.println(" 这是状态 StateD");
		context.setState(new ConcreteStateA());
	}
}
