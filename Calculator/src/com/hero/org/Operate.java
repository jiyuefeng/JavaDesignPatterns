package com.hero.org;
public interface Operate {
	public double getResult(double a, double b);
}
